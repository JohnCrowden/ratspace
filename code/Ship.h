#ifndef SHIP_H
#define SHIP_H

#include "Includes.h"
#include "GameObject.h"
#include "Turret.h"
#include "Cursor.h"
#include "Projectile.h"
#include "Target.h"

class Ship : public GameObject
{
public:
	void Fire();
	void Thrust();
	void TurnLeft();
	void TurnRight();
	virtual void DoUpdate();
	int GetHP();

	Ship(const stringc shipPath,Cursor* cursor=0);
	
	int shootWait;
private:

	vector2df _vel;
	vector2df _burnTrail;
	
	static const int PARTICLES_PER_SECOND = 50;
	static const int MAX_SPEED = 1;
	static const int MAP_SIZE = 300;
	static const int TURN_SPEED = 3;
	static const int TOTAL_BULLETS = 10;
	bool _thrusting;
	scene::IParticleSystemSceneNode* _engine;
	scene::IParticlePointEmitter* _emit;
	ISound* _engineSound;
	Turret* _turret;
	Cursor* _cursor;
	
	int _hp;
	stringc _shipPath;
	ILightSceneNode* _shootLight;
	irr::core::array<Projectile*> _bullets;
	int _nextBullet;
	virtual void OnCollision(GameObject* obj);
};

#endif