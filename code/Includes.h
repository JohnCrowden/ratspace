#ifndef INCLUDES_H
#define INCLUDES_H

#pragma comment(lib, "..\\lib\\Irrlicht\\Irrlicht.lib")
#pragma comment(lib, "..\\lib\\Newton\\Newton.lib")
#pragma comment(lib, "..\\lib\\IrrKlang\\IrrKlang.lib")

#include "..\include\Irrlicht\irrlicht.h"
#include "..\include\Newton\Newton.h"
#include "..\include\IrrKlang\irrKlang.h"
#pragma endregion

#include "..\include\dirent\dirent.h"



using namespace irr;
using namespace irr::core;
using namespace irr::scene;
using namespace irr::video;
using namespace std;
using namespace irrklang;

#endif