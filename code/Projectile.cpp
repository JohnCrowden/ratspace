#include "Includes.h"
#include "Projectile.h"
#include "Ship.h"
#include "Turret.h"

Projectile::Projectile(stringc meshPath)
{
	
	
	 _node = _smgr->addCubeSceneNode(2,0,-1,vector3df(0,0,0),vector3df(0,0,0),vector3df(1,0,1));
	_node->setMaterialTexture(0,_driver->getTexture(meshPath)); 
	_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING, false);
	_node->setMaterialType(E_MATERIAL_TYPE::EMT_TRANSPARENT_ALPHA_CHANNEL);
	_node->setVisible(false);
	
	
		
}

void Projectile::DoUpdate()
{
	f32 zHeight = 2;

	// Firstly, lean is used to calculate the trajectory of the projectile
	vector3df lean = vector3df(sin(this->Angle * PI / 180 ) ,-cos(this->Angle * PI / 180 ),0);
	

	

	this->Position += lean;
	
	

	line3df lin = line3df(this->Position.X,this->Position.Y, zHeight,
		this->Position.X, this->Position.Y,-zHeight);
	
	vector3df outPoint;
	triangle3df outTriangle;
	ISceneNode* collWith = 0;
	collWith =_smgr->getSceneCollisionManager()->getSceneNodeAndCollisionPointFromRay(lin,outPoint,outTriangle);
	
	
	if(collWith)
	{
		
		if(_node->isVisible() && _parentShip->GetNode()->getID() != collWith->getID() )
		{
		GameObject* go = GameObject::GetObjectAt(collWith->getID());
		go->OnCollision(this);
		_node->setVisible(false);
		}
	}
	
	// This is used for debugging (TODO: add switch)
	if(_node->isVisible() && false)
	{
	SMaterial mat;
	mat.Lighting = false;
	mat.Wireframe = true;
	matrix4 tmat;
	_driver->setTransform(ETS_WORLD,tmat);
	_driver->setMaterial(mat);
	_driver->draw3DLine(lin.start,lin.end);
	}
	
}

void Projectile::Fire(Ship* parentShip,Turret* firingTurret)
{
	_parentShip = parentShip;
	this->Position.X = firingTurret->Position.X;
	this->Position.Y = firingTurret->Position.Y;
	this->Position.Z = 0;
	this->Angle = firingTurret->Angle;

	_node->setVisible(true);
}