#include "Includes.h"

#include "EventReceiver.h"

bool EventReceiver::OnEvent(const SEvent& event)	
{
		// Remember whether each key is down or up
		if (event.EventType == irr::EET_KEY_INPUT_EVENT)
			KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;

		if(event.EventType == irr::EET_MOUSE_INPUT_EVENT)
		{
			_mouseInput = event.MouseInput;
			
		}

		if(event.EventType == irr::EET_GUI_EVENT)
		{
			if(event.GUIEvent.Element)
			{
			
				MenuMode = true;
			}
			else
			{
				MenuMode = false;
			
			}
		}
		
		return false;
}

bool EventReceiver::IsKeyDown(EKEY_CODE keyCode) const
{
		return KeyIsDown[keyCode];
}

irr::SEvent::SMouseInput EventReceiver::GetMouseInput() const
{
	return _mouseInput;
}