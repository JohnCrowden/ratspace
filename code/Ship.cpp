#include "Includes.h"
#include "Ship.h"

#include "Ship.h"


Ship::Ship(const stringc shipPath,Cursor* cursor) : GameObject(true)
{

	_cursor = cursor;

	

	IAnimatedMesh* mesh = _smgr->getMesh(shipPath +  "Ship\\Ship.obj");
	
	
	_node  = _smgr->addMeshSceneNode(mesh);
	
	
	 
	
		

		Angle = 90;
		
	_vel = vector2df(0,0);
	_burnTrail = vector2df(0,0);



	_engine = _smgr->addParticleSystemSceneNode(false);
	_emit=  _engine->createPointEmitter(vector3df(0,0,0),
		PARTICLES_PER_SECOND,
		PARTICLES_PER_SECOND,SColor(255,255,255,255),SColor(255,255,255,255),300,300);
	if(_engine)
	{

		_engine->setMaterialFlag(EMF_LIGHTING,false);
		_engine->setMaterialTexture(0,_driver->getTexture(shipPath +"flame.png" ));
		_engine->setMaterialType(EMT_TRANSPARENT_ADD_COLOR);
		_engine->setEmitter(_emit);       
		_engine->setParticlesAreGlobal(false);
		_engine->setParticleSize(dimension2d<f32>(2.0f,2.0f));
		_engine->addAffector(_engine->createFadeOutParticleAffector());

	}
	
	_engineSound = _soundEngine->play2D((shipPath + "engine.mp3").c_str(),true,true);
	
	
	_thrusting = false;

	_turret = new Turret(shipPath + "Turret\\Turret.3ds");

	_shootLight = _smgr->addLightSceneNode(_node,vector3df(0,0,2),SColorf(1.0f,0,0),20.0f);
	_shootLight->setVisible(false);

	
	for(int i = 0;i < TOTAL_BULLETS;i++)
	{
	_bullets.push_back(new Projectile(shipPath + "laser.png"));
	}

	
	
	

	shootWait = 100;
	_nextBullet = 0;
	_shipPath = shipPath;
	
	new Target(shipPath,this);
	_hp= 100;
}





void Ship::Thrust()
{
	_vel.X += sin(Angle * PI / 180 ) / 100;
	_vel.Y -= cos(Angle * PI / 180 ) / 100;
	_emit->setMinParticlesPerSecond(PARTICLES_PER_SECOND);
	_emit->setMaxParticlesPerSecond(PARTICLES_PER_SECOND);
	_engineSound->setIsPaused(false);
	_thrusting = true;
}

void Ship::DoUpdate()
{
	

	if(_vel.X > MAX_SPEED) _vel.X = MAX_SPEED;
	if(_vel.X < -MAX_SPEED) _vel.X = -MAX_SPEED;
	if(_vel.Y > MAX_SPEED) _vel.Y = MAX_SPEED;
	if(_vel.Y < -MAX_SPEED) _vel.Y = -MAX_SPEED;

	// Limit player within the walls of the world
	if(Position.X > MAP_SIZE/2) {_vel.X = -0.1f;}
	if(Position.X < -MAP_SIZE/2) {_vel.X = 0.1f;}
	if(Position.Y > MAP_SIZE/2) {_vel.X = -0.1f;}
	if(Position.Y < -MAP_SIZE/2) {_vel.X = 0.1f;}

	Position.X += _vel.X ;
	Position.Y += _vel.Y ;

	// Fix the ship taking turning the wrong direction (a longer path) when tracking a target
	if(Angle > 360) Angle = 0;
	if(Angle < -360) Angle = 0;


	_burnTrail.X = sin(Angle * PI / 180 ) / 20;
	_burnTrail.Y = -cos(Angle * PI / 180 ) / 20;

	_emit->setDirection(vector3df(-_burnTrail.X,-_burnTrail.Y,0));
	_engine->setPosition(vector3df(
		GetNode()->getPosition().X - _burnTrail.X * 80,
		GetNode()->getPosition().Y - _burnTrail.Y * 80,0));

	
	_turret->Position = vector3df(
		GetNode()->getPosition().X - _burnTrail.X * 20,
		GetNode()->getPosition().Y - _burnTrail.Y * 20,2);
	

	if(_cursor)
	{
		_turret->Angle = _turret->GetTrackingAngle(_cursor);
	}
	else
	{
		_turret->Angle = this->Angle;
	}

	
	if(!_thrusting)
	{
		_emit->setMinParticlesPerSecond(0);
		_emit->setMaxParticlesPerSecond(0);
		_engineSound->setIsPaused(true);
	}

	_thrusting = false;



	shootWait++;

	if(shootWait > 5)
	{
		_shootLight->setVisible(false);
	}

	
}



void Ship::TurnLeft()
{
	Angle -= TURN_SPEED;
}

void Ship::TurnRight()
{
	Angle += TURN_SPEED;
}

void Ship::Fire()
{
	if(shootWait > 30)
	{
	_soundEngine->play2D((_shipPath + "laser.mp3").c_str());
	_shootLight->setVisible(true);
	
	_bullets[_nextBullet]->Fire(this,_turret);

	if(_nextBullet < TOTAL_BULLETS-1)
	{
	_nextBullet++;
	}
	else
	{
		_nextBullet = 0;
	}

	shootWait = 0;
	}
	
}



void Ship::OnCollision(GameObject* obj)
{
	shootWait = 0;
	_hp -= 10;
}

int Ship::GetHP()
{
	return _hp;
}