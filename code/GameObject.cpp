#include "Includes.h"
#include "GameObject.h"


ISoundEngine* GameObject::_soundEngine;
	video::IVideoDriver* GameObject::_driver;
	scene::ISceneManager* GameObject::_smgr;
	scene::ISceneCollisionManager* GameObject::_collmgr;
	gui::IGUIEnvironment* GameObject::_env;
	IrrlichtDevice* GameObject::_device;
	scene::ICameraSceneNode* GameObject::_camera;
	EventReceiver* GameObject::_receiver;
	array<GameObject*> GameObject::_objects = array<GameObject*>();
	int GameObject::_newID;
void GameObject::Update()
{
	if(!_setID)
	{
	_node->setID(_newID++);

	// 3ds format messes up the normals, so turn off culling
	
	_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_FRONT_FACE_CULLING,false);
_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_BACK_FACE_CULLING,false);
	
	//_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING,false);
	if(_collision)
	{
		
	ITriangleSelector* tri = _smgr->createTriangleSelector(_node->getMesh(),_node);
	_node->setTriangleSelector(tri);
	tri->drop();
	}
	_setID = true;
	}
	DoUpdate();

		_node->setPosition(Position);

		
		
		_node->setRotation(vector3df(90,0,Angle));
	
		

}


void GameObject::UpdateAll()
{
	for(int i = 0;i < _objects.size();i++)
	{
		_objects[i]->Update();

	}
}

void GameObject::DropAll()
{
	for(int i = 0;i < _objects.size();i++)
	{	
		_objects[i] = NULL;
		delete _objects[i];

	}
}

IMeshSceneNode* GameObject::GetNode()
{
	return _node;
}

GameObject::GameObject(bool collision)
{
	this->Position = vector3df(0,0,0);
	this->Angle = 0;
	_setID = false;
	_objects.push_front(this);
	_collision = collision;
}

void GameObject::DoUpdate()
{

}



void GameObject::SetupDevice(IrrlichtDevice* device,ICameraSceneNode* camera,EventReceiver* receiver,ISoundEngine* soundEngine)
{
		
	_device = device;
	_driver = device->getVideoDriver();
	_smgr = device->getSceneManager();
	_collmgr = _smgr->getSceneCollisionManager();
	_env = device->getGUIEnvironment();
	_camera = camera;
	_receiver = receiver;
	_soundEngine = soundEngine;
	_newID = 0;
}
	

const float GameObject::GetTrackingAngle(GameObject* obj)
{
		float adj = obj->Position.X - this->Position.X;
	float opp = this->Position.Y - obj->Position.Y;

	int targetAngle = atan2(adj,opp ) * 180 / PI;

	if(targetAngle > 360) targetAngle = 0;

	if(targetAngle < this->Angle - 180) targetAngle += 360;
	if(targetAngle > this->Angle + 180) targetAngle -= 360;

	return targetAngle;
}

void GameObject::OnCollision(GameObject* obj)
{

}

 GameObject* GameObject::GetObjectAt(int index)
 {

	 return _objects[index];
 }