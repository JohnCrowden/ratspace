#ifndef TARGET_H
#define TARGET_H


#include "Includes.h"
#include "GameObject.h"

class Ship;

class Target : public GameObject
{
public:
	
	
	Target(stringc shipPaths,Ship* parentShip);
	virtual void DoUpdate();
	
protected:
	
	Ship* _parentShip;
	IMeshSceneNode* _hpBar;
};

#endif