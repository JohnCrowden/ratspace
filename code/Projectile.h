#ifndef PROJECTILE_H
#define PROJECTILE_H


#include "Includes.h"
#include "GameObject.h"

class Ship;
class Turret;

class Projectile : public GameObject
{
public:
	
	
	Projectile(stringc texturePath);
	void Fire(Ship* parentShip,Turret* firingTurret);
	virtual void DoUpdate();
protected:
	Ship* _parentShip;
	
};

#endif