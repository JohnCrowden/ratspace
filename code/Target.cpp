#include "Includes.h"
#include "GameObject.h"
#include "Target.h"
#include "Ship.h"


Target::Target(stringc shipPath,Ship* parentShip)
{

	_node = _smgr->addCubeSceneNode(12,0,-1,vector3df(0,0,0),vector3df(0,0,0),vector3df(1,0,1));
	_node->setMaterialTexture(0,_driver->getTexture(shipPath + "target.png")); 
	_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING, false);
	_node->setMaterialType(E_MATERIAL_TYPE::EMT_TRANSPARENT_ALPHA_CHANNEL);
		
	_hpBar = _smgr->addCubeSceneNode(12,0,-1,vector3df(0,0,0),vector3df(90,0,0));
	_hpBar->setMaterialTexture(0,_driver->getTexture(shipPath + "hpbar.png")); 
	_hpBar->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING, false);
	_hpBar->setMaterialType(E_MATERIAL_TYPE::EMT_TRANSPARENT_ALPHA_CHANNEL);

	this->Angle = 90;
	_parentShip = parentShip;
}


void Target::DoUpdate()
{
	this->Position = _parentShip->Position;
	this->Position.Y += 1;
			
		_hpBar->setPosition(vector3df(_parentShip->Position.X,_parentShip->Position.Y + 6 ,0));
	_hpBar->setScale(vector3df((float)_parentShip->GetHP() / 100,0,0.1f));
}

