#ifndef CURSOR_H
#define CURSOR_H


#include "Includes.h"
#include "GameObject.h"

class Cursor : public GameObject
{
public:
	
	
	Cursor(stringc texturePath);
	virtual void DoUpdate();
	
protected:
	
	
};

#endif