#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H



#include "Includes.h"
#include "EventReceiver.h"

class GameObject
{
public:

	vector3df Position;
	float Angle;

	// Methods
	void Update();
	IMeshSceneNode* GetNode();
	GameObject(bool collision = false);

	const float GetTrackingAngle(GameObject* obj);

	static void SetupDevice(IrrlichtDevice* device,ICameraSceneNode* camera,EventReceiver* receiver, ISoundEngine* soundEngine);
	static void UpdateAll();
	static void DropAll();
	static GameObject* GetObjectAt(int index);
	virtual void OnCollision(GameObject* obj);
protected:
	IMeshSceneNode* _node;
	virtual void DoUpdate();
	

	static ISoundEngine* _soundEngine;
	static video::IVideoDriver* _driver;
	static scene::ISceneManager* _smgr;
	static scene::ISceneCollisionManager* _collmgr;
	static gui::IGUIEnvironment* _env;
	static IrrlichtDevice* _device;
	static scene::ICameraSceneNode* _camera;
	static EventReceiver* _receiver;
	static int _newID;
	bool _setID;
	bool _collision;
private:
	static irr::core::array<GameObject*> GameObject::_objects;

};

#endif