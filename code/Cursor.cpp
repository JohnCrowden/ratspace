#include "Includes.h"
#include "GameObject.h"
#include "Cursor.h"


Cursor::Cursor(stringc texturePath)
{

	_node = _smgr->addCubeSceneNode(2,0,-1,vector3df(0,0,0),vector3df(0,0,0),vector3df(1,0,1));
	_node->setMaterialTexture(0,_driver->getTexture(texturePath)); 
	_node->setMaterialFlag(E_MATERIAL_FLAG::EMF_LIGHTING, false);
	_node->setMaterialType(E_MATERIAL_TYPE::EMT_TRANSPARENT_ALPHA_CHANNEL);
		
	
	
	
}


void Cursor::DoUpdate()
{
	
				vector2d<s32> bob(_receiver->GetMouseInput().X,_receiver->GetMouseInput().Y);
				line3d<f32> jim = _smgr->getSceneCollisionManager()->getRayFromScreenCoordinates(bob,_camera);
		this->Position = vector3df(jim.end.X / 50 + _camera->getPosition().X,jim.end.Y / 50 + _camera->getPosition().Y,10);

		_node->setVisible(!_receiver->MenuMode);
		
		
		

		this->Angle++;
		
}

