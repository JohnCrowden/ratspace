
// We'll also define this to stop MSVC complaining about sprintf().
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Includes.h"

#include <math.h>
#include <iostream>
#include <array>
#include <vector>
#include <time.h>
#include "EventReceiver.h"
#include "Ship.h"
#include "Wall.h"


static ISoundEngine* soundEngine;
 static video::IVideoDriver* driver;
 static scene::ISceneManager* smgr;
 static scene::ISceneCollisionManager* collmgr;
static gui::IGUIEnvironment* env;
static IrrlichtDevice* device;


int main()
{
	float cursorRot = 0;
	const int MAP_SIZE = 300;
	
	const stringc CONTENT_DIR = "..\\content\\";
	const stringc SHIP_DIR = CONTENT_DIR + "ships\\";
	const stringc TEXTURE_DIR = CONTENT_DIR + "textures\\";
	const stringc MUSIC_DIR = CONTENT_DIR + "music\\";
	const stringc SOUND_DIR = CONTENT_DIR + "sounds\\";
	const stringc FONT_DIR = CONTENT_DIR + "fonts\\";
	srand ( time(NULL) );


	 soundEngine = createIrrKlangDevice();

	// Play the user a random song
	std::vector<stringc> songs;
	DIR *dir;
	struct dirent *ent;
	dir = opendir(MUSIC_DIR.c_str());
	if(dir != NULL)
	{
		// Go through the music directory and get their names
		while((ent = readdir(dir)) != NULL)
		{
			// This is stop .. and . being added.
			if(strlen(ent->d_name) > 2)
			{
			songs.push_back(ent->d_name);
			}
		}
		closedir(dir);
	}
	else
	{
		cout << "Could not open directory '" << MUSIC_DIR.c_str() << "'";
	}
	
	// Play a random song
	//soundEngine->play2D((MUSIC_DIR + songs[rand() % songs.size()].c_str()).c_str(),true);

	
	// create device
	EventReceiver receiver;

	device = createDevice(video::E_DRIVER_TYPE::EDT_DIRECT3D9,
		core::dimension2d<u32>(1024, 768), 32, false, false, true, &receiver);
	
	if (device == 0)
		return 1; // could not create selected driver.

	

	driver = device->getVideoDriver();
	smgr = device->getSceneManager();
	collmgr = smgr->getSceneCollisionManager();
	env = device->getGUIEnvironment();
	
	
	
	gui::IGUIWindow* win = env->addWindow(rect<s32>(0,0,200,9000),false);
	win->setDrawTitlebar(false);
	win->getCloseButton()->setVisible(false);
	win->setID(44);
	win->setDraggable(false);

	gui::IGUIButton* but = env->addButton(rect<s32>(50,50,180,70),0,-1,L"Bacon Biscuit");
	but->setOverrideFont(env->getFont(FONT_DIR + "RatFont.xml"));
	
	
	std::array<scene::ISceneNode*,20> planets; 



	for(u32 i = 0;i < planets.size();i++)
	{
		planets[i] = smgr->addSphereSceneNode();
		// rand returns positive numbers
		f32 spreadX = rand() % MAP_SIZE - (MAP_SIZE / 2); 
		f32 spreadY = rand() % MAP_SIZE - (MAP_SIZE / 2); 
		f32 spreadZ = rand() % 30;
		if (planets[i])
		{
			planets[i]->setPosition(core::vector3df(spreadX,spreadY,spreadZ));
			planets[i]->setMaterialTexture(0, driver->getTexture(TEXTURE_DIR + "earth.jpg"));
			planets[i]->setMaterialFlag(video::EMF_LIGHTING, true);
		}
	}

	/*
	std::array<scene::ISceneNode*,30> stars; 



	for(u32 i = 0;i < stars.size();i++)
	{
		stars[i] = smgr->addSphereSceneNode();
		// rand returns positive numbers
		f32 spreadX = rand() % MAP_SIZE - (MAP_SIZE / 2); 
		f32 spreadY = rand() % MAP_SIZE - (MAP_SIZE / 2); 
		f32 spreadZ = 0;
		if (stars[i])
		{
			stars[i]->setPosition(core::vector3df(spreadX,spreadY,-spreadZ));
			
			stars[i]->setMaterialFlag(video::EMF_LIGHTING, false);
			
		}
	}

	*/
	scene::IMeshSceneNode* sun = smgr->addSphereSceneNode();
	if (sun)
	{

		sun->setPosition(core::vector3df(0,50,0));
		sun->setMaterialTexture(0, driver->getTexture(TEXTURE_DIR + "sun.jpg"));
		sun->setMaterialFlag(video::EMF_LIGHTING, false);
		

	}
	
	
	

	smgr->addLightSceneNode(sun);
	
	f32 lightness = 0.5f;
	smgr->setAmbientLight(SColorf(lightness,lightness,lightness,lightness));

	ICameraSceneNode* camera = smgr->addCameraSceneNode(0,irr::core::vector3df(0,0,20),vector3df(0,0,0));

	GameObject::SetupDevice(device,camera,&receiver,soundEngine);

	Cursor* cursor = new Cursor(TEXTURE_DIR + "cursor.png");
	

	Ship* player = new Ship(
		SHIP_DIR + "Miner\\",cursor);
	
	Ship* enemy = new Ship(
		SHIP_DIR + "Miner\\");
	
	enemy->Position.X = 20;

	player->Position.X = -20;
	
	
	
	Wall* wall = new Wall();

	
	while(device->run())
	{

		if(receiver.GetMouseInput().isLeftPressed())
		{

			player->Fire();
		}

		if(receiver.IsKeyDown(irr::KEY_KEY_W))
		{
		  player->Thrust();	
		}
	

		if(receiver.IsKeyDown(irr::KEY_KEY_A))
		{
			player->TurnLeft();
		}
		
		if(receiver.IsKeyDown(irr::KEY_KEY_D))
		{
			player->TurnRight();
		}


			if(receiver.IsKeyDown(irr::KEY_UP))
		{
		  enemy->Thrust();	
		}
	
			

			int diffPos = enemy->Angle- enemy->GetTrackingAngle(player);

	if(diffPos > 0)
		{
			enemy->TurnLeft();
		}

		
		if(diffPos < 0)
		{
			enemy->TurnRight();
		}

		// TODO: Make AI a bit smarter with the use of thrusters
		//
		
		
			if( enemy->Position.getDistanceFrom(player->Position) > 80)
			{
			//	enemy->Thrust();
			}
			if( enemy->Position.getDistanceFrom(player->Position) < 50)
			{
			//	enemy->Fire();
			}
		

		if(receiver.IsKeyDown(irr::KEY_ESCAPE))
		{
			device->closeDevice();
		}

	
		device->getCursorControl()->setVisible(receiver.MenuMode);
		

	

		camera->setTarget(player->Position);
		camera->setPosition(vector3df(player->Position.X,player->Position.Y,50));

		
		
		
		
		

		driver->beginScene(true, true, video::SColor(0,0,0,0));
		GameObject::UpdateAll();
		smgr->drawAll(); // draw the 3d scene
		device->getGUIEnvironment()->drawAll(); 
		
		
	
		driver->endScene();




		core::stringw tmp(L"RatSpace [ ");
		tmp += L"FPS: ";
		tmp += driver->getFPS();
		tmp += ", shootWait: ";
		tmp += player->shootWait;
		tmp += "]";

		device->setWindowCaption(tmp.c_str());

	}


	
	GameObject::DropAll();
	device->drop();
	soundEngine->drop();
	return 0;
}

