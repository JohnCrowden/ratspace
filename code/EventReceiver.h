#ifndef EVENT_RECEIVER_H
#define EVENT_RECEIVER_H

#include "Includes.h"

class EventReceiver : public IEventReceiver
{
public:
	
	virtual bool OnEvent(const SEvent& event);


	
	virtual bool IsKeyDown(EKEY_CODE keyCode) const;
	
	irr::SEvent::SMouseInput GetMouseInput() const;

	EventReceiver()
	{
		for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i)
			KeyIsDown[i] = false;

		MenuMode = false;
	}

	bool MenuMode;
private:
	
	bool KeyIsDown[KEY_KEY_CODES_COUNT];
	irr::SEvent::SMouseInput _mouseInput;
};

#endif