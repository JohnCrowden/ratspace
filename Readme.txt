Hello there!

I compiled this project in Visual Studio 2010 Express, although I've tried to make sure as little settings as possible are stored in the project settings so you can use your own IDE.

It also uses Irrlicht as a rendering engine, so far its hardcoded to use DirectX9, but this can be changed really easily.

Take a look at issues for what you need to do  

As general coding style, local variables and parameter names should be camelCase, properties and functions should be PascalCase and finally private variables should be prefixed with an underscore e.g. _position. 
And of course... NO HUNGARIAN NOTATION 

For a non-technical overview of what the game is about, take a look in \docs